INTRODUCTION
------------

Based on the core statistics module, which tracks general page views and counts node views. It is also a replacement for/upgrade to module term_statistics(https://www.drupal.org/project/term_statistics).

The module counts views of taxonomy terms and integrates nicely with the core statistics module. Any configuration settings are added to the statistics configuration settings form. It includes a separate block for top-viewed categories similar to the statistics module. It includes views field, filter and sort support. There is also an option to do the counting asynchronous through ajax.


REQUIREMENTS
------------

This module requires the core statistic module:


RECOMMENDED MODULES
-------------------

 * Taxonomy stats (https://www.drupal.org/project/taxonomy_stats):
 * Entity statistics (https://www.drupal.org/project/entity_statistics)


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 
 * To enable collection of statistics, enable the "Enable access log" on the Statistics settings page ( 
   Administration » Configuration » Statistics ).

 * To use the counter, enable the "Count taxonomy term views" on the statistics settings page( Administration » 
   Configuration » Statistics ), and set the necessary permissions (View taxonomy term hits) so that the counter is visible to the users.

 * To display the block of Popular taxonomy terms, enable this block on Block administration page ( Administration » 
   Structure » Blocks)


TROUBLESHOOTING
---------------

 * If the counter is not displayed, check the following:

   - Is the "View taxonomy term hits" permission enabled for the appropriate roles?


MAINTAINERS
-----------

Current MAINTAINERS:
 * Jan van Diepen - https://www.drupal.org/u/jan-van-diepen

This project has been sponsored by:
 * NDI Tech 
   The NDItech blog provides a platform for NDI to engage in ongoing conversations about the important and increasing role technology plays in politics and democratic development

 * National Democratic Institute
   The National Democratic Institute is a nonprofit, nonpartisan organization working to support and strengthen democratic institutions worldwide through citizen participation, openness and accountability in government.

